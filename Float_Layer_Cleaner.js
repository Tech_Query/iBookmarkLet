//
//    1Key Web Float Layer Cleaner   v0.5
//
//                2014-12-04
//
//        (C)2014  test_32@fyscu.com
//

(function (WO, DO) {
    function $_TN(REO, TagName) {
        return  [].slice.apply( REO.getElementsByTagName(TagName) );
    }
    function get_Style(_TE, _SN) {
        var _Style = _TE.currentStyle ?
            _TE.currentStyle.getAttribute(_SN) :
            DO.defaultView.getComputedStyle(_TE, null).getPropertyValue(_SN);
        var _Number = Number(
            (_Style == '0px') ? 0 : _Style
        );
        return  isNaN(_Number) ? _Style : _Number;
    }
    function CSS_Match(EO, RG) {
        for (var i = 0; i < RG.length; i++)
            if (get_Style(EO, RG[i][0]) == RG[i][1])
                return 1;
    }
    function ParentMatch(EO, Parent, Level) {
        for (var i = 0; i < Level; i++)  try {
            if (EO.parentNode === Parent)
                return 1;
            else if (EO.parentNode === DO)
                break;
            else  EO = EO.parentNode;
        } catch (iError) {
            console.log(iError);
        }
    }
    function isFixedLayer(_TE) {
        var inVisible = CSS_Match(_TE, [
            ['display',     'none'],
            ['visibility',  'hidden'],
            ['width',       0]
        ]);
        var Fixed = (! inVisible) && CSS_Match(_TE, [
            ['position',  'absolute'],
            ['position',  'fixed']
        ]);
        if (Fixed && (
            WO.FLC ? true : (get_Style(_TE, 'z-index') > 100)
        ))
            return ParentMatch(
                //  清除效果 基本只取决于“DOM 遍历深度”
                _TE,  DO.body,  (WO.FLC ? 13 : 3)
            );
    }
    var FL_Box = $_TN( $_TN(DO, 'body')[0], '*');
    for (var iKey = 0, FL_Count = 0, _FL; iKey < FL_Box.length; iKey++) {
        _FL = FL_Box[iKey];
        if ( isFixedLayer(_FL) ) {
            _FL.parentNode.removeChild(_FL);
            console.log(_FL);
            FL_Count++;
        }
    }
    if (! self.FLC)  self.FLC = true;
    alert([
        FL_Count,  ' of the fucking Float ADs/Layers have been cleaned !~',
        '\n',  'You can try Clicking my Button again to be cleaner.',
        '\n\n',  '(C)2014   SCU FYclub-RDD'
    ].join(''));
})(self, self.document);