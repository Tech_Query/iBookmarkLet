//
//                >>>  HTML Injector  <<<
//
//
//      [Version]    v0.4  (2016-3-30)  Beta
//
//      [Require]    JavaScript v1.5+  (ECMAScript 3)
//
//      [Usage]      Inject HTML Code into a DOM tree.
//
//
//          (C)2014-2016    shiy2008@gmail.com
//


javascript:  (function (BOM) {

/* ---------- ClipBoard Paste Data ---------- */

    var isIE = !(! BOM.document.attachEvent),
        MIME_Type = {
            text:    'text/plain',
            url:     'text/unicode',
            html:    'text/html'
        };
    var Event_Method = isIE ? [
            'attachEvent', 'onpaste', 'detachEvent'
        ] : [
            'addEventListener', 'paste', 'removeEventListener'
        ];

    BOM.getPaste  =  BOM.getPaste  ||  function (cType, CallBack) {
        var _BOM_ = this;
        cType = isIE ? cType : MIME_Type[cType.toLowerCase()];

        _BOM_.document[ Event_Method[cType ? 0 : 2] ](
            Event_Method[1],
            function () {
                var iRoot = arguments[0] || _BOM_;

                if (false === CallBack.call(
                    iRoot.target || iRoot.event.srcElement,
                    iRoot.clipboardData.getData(cType)
                )) {
                    if (isIE)
                        iRoot.event.returnValue = false;
                    else
                        iRoot.preventDefault();
                }
            },
            false
        );
    };
/* ---------- Window Frame Traversing ---------- */

    BOM.getAllFrames  =  BOM.getAllFrames  ||  function () {
        var iSet = [ ],  iFrame = this.frames;

        for (var i = 0, j = 0;  i < iFrame.length;  i++) {
            iSet[j++] = iFrame[i];

            if (iFrame[i].frames.length)
                iSet = iSet.concat( arguments.callee.call( iFrame[i] ) );
        }

        return iSet;
    };

    BOM.eachFrame  =  BOM.eachFrame  ||  function () {
        var iFrame = [this].concat( BOM.getAllFrames.call(this) );

        for (var i = 0, _Return_; i < iFrame.length; i++)  try {

            _Return_ = arguments[0].call(iFrame[i], iFrame[i].document);

            if (_Return_ === false)  break;

            if (_Return_ !== undefined)  return _Return_;

        } catch (iError) { }
    };

/* ---------- HTML Injection ---------- */

    BOM.HTML_Inject  =  BOM.HTML_Inject  ||  function (iHTML_Fragment) {
        var Target_Tag = BOM.eachFrame(function (_DOM_) {
                var This_Tag = _DOM_.activeElement;

                if (! This_Tag)  return false;

                switch (This_Tag.tagName.toLowerCase()) {
                    case 'body':      ;
                    case 'input':     ;
                    case 'select':    return;
                    case 'iframe':    {
                        if (This_Tag.contentWindow.frames.length)  return;

                        This_Tag = This_Tag.contentWindow.document.body;
                    }
                }
                return This_Tag;
            });

        if (! Target_Tag)
            return BOM.alert(
                "未找到可用的插入点……\n\n请在要插入代码的编辑框中单击鼠标，再点击本工具~"
            );
        Target_Tag.innerHTML = iHTML_Fragment ||
            BOM.prompt("请输入要插入的 HTML 代码片段");

        if (Target_Tag.innerHTML)  return;

        if (! Target_Tag.iWatch) {
            BOM.getPaste.call(
                Target_Tag.ownerDocument.defaultView,
                'Text',
                function (iHTML) {
                    Target_Tag.innerHTML = iHTML;
                    return false;   
                }
            );
            Target_Tag.iWatch = true;
        }
        BOM.alert("本次没有输入，但之后您可以直接在目标编辑框中粘贴（无需再次点击本工具）~");
    };

    BOM.HTML_Inject();

})(self);