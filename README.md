# iBookmarkLet

**Web 2.0** 时代，各种互联网公司都想靠 **网页浏览器** 来圈地、圈钱，争相推出自己的原创/马甲浏览器，功能或简或繁，但总有些 **实用功能**缺这少那…… 而且即便内核相同，扩展插件的 API 也互不兼容，一个实用插件往往要为有较大用户基数的浏览器维护不同的源码、安装包……

　　所以，若 **标准 HTML/CSS/JavaScript** 能解决的问题，我们就可以用 `javascript:` 这样的通用协议，把 JS 插件封装成一个 **收藏栏按钮**，每次只需轻轻一点~

　　如果 **Bookmarklet** 这样的空间、权限有限的技术很难满足新需求，那本项目的程序源码也可以作为各种版本浏览器插件的蓝本（核心逻辑）来用~


　【注】本项目的安装代码均使用 [UglifyJS](http://www.oschina.net/p/uglifyjs) 压缩


## 【网页浮层一键清除】v0.5

**发布博文** —— http://log.fyscu.com/index.php/archives/118/

### 主要特性
 1. 可清除 **网页中任一位置的浮动广告**
 2. 可清除 **遮蔽整个网页的登录框**
 3. 可清除 **笼罩整个网页的透明弹窗广告超链接**
 4. 可清除 **网页正文图片边缘的浮动广告**（一般在第二次点击本工具时成功，但此时会误杀一些网页本身的功能浮层，可能会影响某些功能的使用， **建议在纯阅读时二次点击本工具**）

### 安装代码

    javascript: (function(a,b){function c(a,b){return[].slice.apply(a.getElementsByTagName(b))}function d(a,c){var d=a.currentStyle?a.currentStyle.getAttribute(c):b.defaultView.getComputedStyle(a,null).getPropertyValue(c),e=Number("0px"==d?0:d);return isNaN(e)?d:e}function e(a,b){for(var c=0;c<b.length;c++)if(d(a,b[c][0])==b[c][1])return 1}function f(a,c,d){for(var e=0;d>e;e++)try{if(a.parentNode===c)return 1;if(a.parentNode===b)break;a=a.parentNode}catch(f){console.log(f)}}function g(c){var g=e(c,[["display","none"],["visibility","hidden"],["width",0]]),h=!g&&e(c,[["position","absolute"],["position","fixed"]]);return h&&(a.FLC?!0:d(c,"z-index")>100)?f(c,b.body,a.FLC?13:3):void 0}var k,i,j,h=c(c(b,"body")[0],"*");for(i=0,j=0;i<h.length;i++)k=h[i],g(k)&&(k.parentNode.removeChild(k),console.log(k),j++);self.FLC||(self.FLC=!0),alert([j," of the fucking Float ADs/Layers have been cleaned !~","\n","You can try Clicking my Button again to be cleaner.","\n\n","(C)2014   SCU FYclub-RDD"].join(""))})(self,self.document);


## 【Google 中文划词搜索】v1.3

**发布博文** —— http://log.fyscu.com/index.php/archives/210/

### 主要特性
 1. 点击本工具收藏栏按钮时，将对网页中选中的文字使用 **Google 中文搜索**
 2. 自动随机选用最优秀的 Google 海外镜像站， **用户无需翻墙就能获得快速、稳定、原生的搜索体验**
 3. 搜索结果集页面在新网页中打开，具体的结果网页也会打开新网页，不影响原网页的使用
 4. **默认只搜索简体中文网页**，省去使用“高级搜索工具”筛选本地化网页的麻烦

### 安装代码

    javascript: (function(BOM){function Random_SN(a){return Math.floor(Math.random()*a.length)}function Probability_Random(a){var b;void 0===a.Random_SN&&(a.Random_SN=-1);do b=Random_SN(a);while(b==a.Random_SN);return a.Random_SN=b,a[b]}function Sequence_Random(a){return a.splice(Random_SN(a),1)[0]}Array.prototype.random=Array.prototype.random||function(a,b){return a?b?Sequence_Random(this):(this.Random_Queue&&this.Random_Queue.length||(this.Random_Queue=[].concat(this)),Sequence_Random(this.Random_Queue)):Probability_Random(this)},String.prototype.trim=String.prototype.trim||function(){return this.replace(/(^\s*)|(\s*$)/g,"")},BOM.getAllFrames=BOM.getAllFrames||function(){var c,d,a=[],b=this.frames;for(c=0,d=0;c<b.length;c++)a[d++]=b[c],b[c].frames.length&&(a=a.concat(arguments.callee.call(b[c])));return a},BOM.eachFrame=BOM.eachFrame||function(){var c,b,a=[this].concat(BOM.getAllFrames.call(this));for(b=0;b<a.length;b++)try{if(c=arguments[0].call(a[b],a[b].document),c===!1)break;if(void 0!==c)return c}catch(d){}},BOM.getSelectedText=BOM.getSelectedText||function(){return BOM.eachFrame.call(this,function(_DOM_){var This_Tag=_DOM_.activeElement;if(_DOM_.selection)return _DOM_.selection.createRange().text;switch(This_Tag.tagName.toLowerCase()){case"input":case"textarea":with(This_Tag)return value.slice(selectionStart,selectionEnd);case"body":default:return _DOM_.getSelection().toString()}})}}(top),function(a,b,c){var d=a.getSelectedText().trim();return d?a.open(["http://",c.random(!0),"/search?newwindow=1&lr=lang_",b,"&q=",a.encodeURIComponent(d)].join(""),"_blank"):(a.confirm(["您未选中任何网页中的文字……","『确定』进入问题反馈；『取消』即退出本工具。"].join("\n\n\n"))?(a.prompt("输入框中的是『运行环境』信息，请直接复制它们，按『确认』即可访问 原作者主页～",navigator.userAgent),a.open("http://www.fyscu.com/","_blank")):a.alert("【Google 中文划词搜索 v1.3】\n\n(C)2013-2016  四川大学·飞扬俱乐部·研发部"),void 0)})(top,"zh-CN",["sssis.com","www.90r.org","booo.so","g.ttlsa.com","lamjoeone.info","g.net.co","iguge.tk","jiong.lu","xie.lu","ggncr.com","everthis.com","ppx.pw","gugesou.com","soso.red","521.pt","google.sqzr.cc","yue.pao.im","da.pao.im","g.xjliao.me","gg.eeload.com","repigu.com","jinke.la","glgoo.com","googleforchina.com","74.125.12.198","209.116.186.219","173.194.121.28","g.hi18.cn"]);


## 【Google 站内搜索】v1.4

**发布博文** —— http://log.fyscu.com/index.php/archives/211/

### 主要特性
 1. 点击本工具收藏栏按钮时，将对网页中选中的文字使用 **Google** 进行 **站内搜索**
 2. 自动随机选用最优秀的 Google 海外镜像站， **用户无需翻墙就能获得快速、稳定、原生的搜索体验**
 3. 搜索结果集页面在新网页中打开，具体的结果网页也会打开新网页，不影响原网页的使用
 4. 多次点击时可在当前域名上 **回溯搜索父域**

### 安装代码

    javascript: (function(BOM){function Random_SN(a){return Math.floor(Math.random()*a.length)}function Probability_Random(a){var b;void 0===a.Random_SN&&(a.Random_SN=-1);do b=Random_SN(a);while(b==a.Random_SN);return a.Random_SN=b,a[b]}function Sequence_Random(a){return a.splice(Random_SN(a),1)[0]}Array.prototype.random=Array.prototype.random||function(a,b){return a?b?Sequence_Random(this):(this.Random_Queue&&this.Random_Queue.length||(this.Random_Queue=[].concat(this)),Sequence_Random(this.Random_Queue)):Probability_Random(this)},String.prototype.trim=String.prototype.trim||function(){return this.replace(/(^\s*)|(\s*$)/g,"")},BOM.getAllFrames=BOM.getAllFrames||function(){var c,d,a=[],b=this.frames;for(c=0,d=0;c<b.length;c++)a[d++]=b[c],b[c].frames.length&&(a=a.concat(arguments.callee.call(b[c])));return a},BOM.eachFrame=BOM.eachFrame||function(){var c,b,a=[this].concat(BOM.getAllFrames.call(this));for(b=0;b<a.length;b++)try{if(c=arguments[0].call(a[b],a[b].document),c===!1)break;if(void 0!==c)return c}catch(d){}},BOM.getSelectedText=BOM.getSelectedText||function(){return BOM.eachFrame.call(this,function(_DOM_){var This_Tag=_DOM_.activeElement;if(_DOM_.selection)return _DOM_.selection.createRange().text;switch(This_Tag.tagName.toLowerCase()){case"input":case"textarea":with(This_Tag)return value.slice(selectionStart,selectionEnd);case"body":default:return _DOM_.getSelection().toString()}})}}(top),function(a,b,c){var d=a.getSelectedText().trim();return d?(a.open(["http://",c.random(!0),"/search?newwindow=1&q=",a.encodeURIComponent([d," site:",b.domain].join(""))].join(""),"_blank"),b.domain.match(/^[^\.]+\.(edu|net|org|com|gov)(\.[^\.]+)?$/)||(b.domain=b.domain.replace(/^[^\.]+\./,"")),void 0):(a.confirm(["您未选中任何网页中的文字……","『确定』进入问题反馈；『取消』即退出本工具。"].join("\n\n\n"))?(a.prompt("输入框中的是『运行环境』信息，请直接复制它们，按『确认』即可访问 原作者主页～",a.navigator.userAgent),a.open("http://www.fyscu.com/","_blank")):a.alert("【Google 站内搜索工具 v1.4】\n\n(C)2013-2016  四川大学·飞扬俱乐部·研发部"),void 0)})(top,top.document,["wen.lu","awk.so","sssis.com","www.90r.org","gu1234.com","booo.so","www.souji8.com","hk.souji8.com","g.ttlsa.com","gvgle.com"]);


## 【网址回溯浏览】v0.1

**发布博文** —— [http://bbs.fyscu.com/forum.php?mod=viewthread&tid=4685

### 主要特性
 1. 每次点击本工具时，浏览器都将 **沿着当前网址的路径去访问上一级所指向的网络资源**
 2. 一些较新的网站 启用了 **URL Rewrite** 或 **路由控制** 机制，某部分临近的两级路径 可能 指向完全相同，回溯后又会被重定向回来，所以看起来 网址好像没有变化……
 3. 一些浏览器在网页打不开（ **该页无法显示** 等）时，可能无法正常回溯 —— 因为它们此时 地址栏中显示的原网址已不是 window.location 的值，此对象的值已被改为 **浏览器自身报错页面的 URL**~

### 安装代码

    javascript: (function(a){var c,d,b=a.location.href.split("/");if(!b[0].match(/^(http|https|file|ftp):$/))return alert("当前网址所指向的不是有效的网络资源，无法回溯……")||!1;if(c=b.length,""===b[c-1]&&(b.pop(),c--),c>3)b.splice(-1,1);else{if("www."===b[2].slice(0,4))return alert("当前网址已是该网站的顶端，不能再回溯了~");d=b[2].split("."),d.splice(0,1),d.length<3&&d.splice(0,0,"www"),b[2]=d.join(".")}a.location.href=b.join("/")})(top);


## 【网页代码 行内化】v0.2

**发布博文** —— http://my.oschina.net/TechQuery/blog/638707

### 主要特性
 1. 支持 **图片文字水印**，方便 **论坛发帖**、 **微信公众号文章编辑**（配合使用下面的 HTML 代码插入工具）
 2. 若网页中有较多、较大图片，编码时间会略长
 3. 较老的浏览器内核可能会产生较多的 **内联样式**代码

### 安装代码

    javascript: (function(a,b){function c(a){return JSON.parse(JSON.stringify(a.ownerDocument.defaultView.getComputedStyle(a)))}function f(a){var f,b=e.contentWindow.document;return d[a]||(f=b.createElement(a),b.body.appendChild(f),d[a]=c(f),b.body.removeChild(f)),d[a]}function g(){var e,a={},b=c(arguments[0]),d=f(arguments[0].tagName.toLowerCase());for(e in b)!isNaN(Number(e))||e.match(/^(moz|webkit|ms)/)||b[e]==d[e]||e.match(/width|height/i)||(a[e]=b[e]);return a}var d={},e=b.createElement("iframe");e.style.display="none",b.body.appendChild(e),a.CSS_Inline=function(){var b,a=g(arguments[0]);for(b in a)arguments[0].style[b]=a[b]},a.Image_Inline=function(a,c){var f,d=new Image,e=b.createElement("canvas");d.crossOrigin="",f=e.getContext("2d"),d.onload=function(){e.width=d.width,e.height=d.height,f.drawImage(d,0,0),c&&(f.font="20px sans-serif",f.fillStyle="white",f.fillText(c,10,d.height-15)),a.src=e.toDataURL("image/png"),d=null},d.src=a.src},a.Web_Inline=function(){var b,a=arguments[0].querySelectorAll("*");for(b=0;b<a.length;b++)switch(a[b].tagName.toLowerCase()){case"meta":case"style":case"script":case"iframe":a[b].parentNode.removeChild(a[b]);break;case"img":this.Image_Inline(a[b],arguments[1]);break;default:this.CSS_Inline(a[b])}return arguments[0].innerHTML.trim()},a.Web_Inline(b.body,a.prompt("图片水印文字：")),a.setTimeout(function(){b.body.textContent=b.body.innerHTML,a.alert("请全选、复制当前显示的所以代码~")},1e3)})(self,self.document);


## 【富文本编辑框 HTML 代码注入工具】v0.4

**发布博文** —— http://my.oschina.net/TechQuery/blog/350954

### 主要特性
 1. 在没有 **HTML 代码编辑模式**的富文本编辑框中单击，再单击本工具启动按钮，即可把 **自定义 HTML 代码**所能构建的 **网页文档片段**插入进去
 2. **自动检测当前焦点**所在的富文本编辑框（兼容 `<textarea />`、`<iframe />`、`<div />` 等可编辑容器）
 3. 用户错过代码输入后无需再次点击本工具，直接在目标编辑框中粘贴代码即可~（本条不适用于 目标不同 或 页面刷新）
 4. 若网站 **富文本编辑器** 或 服务器后台程序 过滤了某些 HTML 标签，则本工具无能为力……

### 安装代码

    javascript: (function(a){var b=!!a.document.attachEvent,c={text:"text/plain",url:"text/unicode",html:"text/html"},d=b?["attachEvent","onpaste","detachEvent"]:["addEventListener","paste","removeEventListener"];a.getPaste=a.getPaste||function(a,e){var f=this;a=b?a:c[a.toLowerCase()],f.document[d[a?0:2]](d[1],function(){var c=arguments[0]||f;!1===e.call(c.target||c.event.srcElement,c.clipboardData.getData(a))&&(b?c.event.returnValue=!1:c.preventDefault())},!1)},a.getAllFrames=a.getAllFrames||function(){var c,d,a=[],b=this.frames;for(c=0,d=0;c<b.length;c++)a[d++]=b[c],b[c].frames.length&&(a=a.concat(arguments.callee.call(b[c])));return a},a.eachFrame=a.eachFrame||function(){var d,c,b=[this].concat(a.getAllFrames.call(this));for(c=0;c<b.length;c++)try{if(d=arguments[0].call(b[c],b[c].document),d===!1)break;if(void 0!==d)return d}catch(e){}},a.HTML_Inject=a.HTML_Inject||function(b){var c=a.eachFrame(function(a){var b=a.activeElement;if(!b)return!1;switch(b.tagName.toLowerCase()){case"body":case"input":case"select":return;case"iframe":if(b.contentWindow.frames.length)return;b=b.contentWindow.document.body}return b});return c?(c.innerHTML=b||a.prompt("请输入要插入的 HTML 代码片段"),c.innerHTML||(c.iWatch||(a.getPaste.call(c.ownerDocument.defaultView,"Text",function(a){return c.innerHTML=a,!1}),c.iWatch=!0),a.alert("本次没有输入，但之后您可以直接在目标编辑框中粘贴（无需再次点击本工具）~")),void 0):a.alert("未找到可用的插入点……\n\n请在要插入代码的编辑框中单击鼠标，再点击本工具~")},a.HTML_Inject()})(self);


## 【CSS 规则 当前页使用率 检测】v0.1

**发布博文** —— http://log.fyscu.com/index.php/archives/125/

### 主要特性
 1. **扫描当前网页中所有内置/外置的 CSS，在控制台显示各 CSS 标签/文件中哪些规则对本页无用、共占比多少**
 2. 外置 CSS 会显示 **文件相对路径**
 3. 内置 CSS 会显示其对应的 HTML 元素，便于 **在调试器显示的 DOM 树上定位**
 4. **CSS 媒体查询、字体**的规则会在显示的对象中有独立分支
 5. 暂不支持 CSS 选择符中间有 **伪类、伪元素**的情况，只支持它们出现在 选择符链的最后端（这是与下文提及的专业工具之 **有效比计算**有误差的主要原因）
 6. **跨域的 CSS 文件**无法扫描（它们一般都是 **CDN 通用库** 或 部署环境中的 **静态文件服务器**托管的压缩版，本来就不是用于调试的）

### 安装代码
    javascript: (function(a,b,c){function f(a){return a.match(/^\w+:\/\//)&&(a=a.split("/").slice(3),a.unshift("."),a=a.join("/")),a}function g(a){var b,d;for(a=a.split(","),b=[],d=0;d<a.length;d++)c(a[d].trim().split(":")[0]).length||b.push(a[d]);return b}function h(a){var b,e,f,i,h;try{b=a.cssRules||a.rules}catch(c){}if(e=[],f={media:a.media.mediaText,mediaRules:[],fontsRules:[]},b){for(h=0;h<b.length;h++)switch(i=b[h],i.type){case 1:i=g(i.selectorText),i.length&&e.push(i);break;case 4:f.mediaRules.push(arguments.callee(i)[0]);break;case 5:f.fontsRules.push({fontFamily:i.style.fontFamily,src:i.style.src})}f.WasteRate=(100*(e.length/h)).toFixed(2)+"%"}return[f,e]}var j,i,k,d=b.styleSheets,e="(C)2014-2015  test_32@fyscu.com";if(!c)return a.alert("Please run This Tool in IE 8+ (Standard Mode) or a Modern Web Browser.\n\n"+e),!1;if(!console)return a.alert("Please run This Tool with JavaScript Console opened.\n\n"+e),!1;for(i=0,k=[];i<d.length;i++)k[i]=h(d[i]),j=k[i].shift(),j.href=d[i].href,j.WasteRate&&j.href&&(j.href=f(j.href)),j.element=d[i].ownerNode,console.log(j);console.log(k)})(self,self.document,self.document.querySelectorAll);