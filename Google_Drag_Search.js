//
//            >>>  Google Drag Search  <<<
//
//
//      [Version]    v1.3  (2016-3-30)  Beta
//
//      [Require]    JavaScript v1.5+  (ECMAScript 3)
//
//      [Usage]      Search Selected Words by
//
//                   Google Mirror Sites.
//
//
//          (C)2013-2016    shiy2008@gmail.com
//


javascript:  (function (BOM) {

/* ----------- Array Random Element ----------- */

    function Random_SN(iArray) {
        return  Math.floor(Math.random() * iArray.length);
    }

    function Probability_Random(iArray) {
        var Random_Int;

        if (iArray.Random_SN === undefined)
            iArray.Random_SN = -1;

        do  Random_Int = Random_SN(iArray);
        while ( Random_Int == iArray.Random_SN )

        iArray.Random_SN = Random_Int;

        return iArray[Random_Int];
    }

    function Sequence_Random(iArray) {
        return  iArray.splice(Random_SN(iArray), 1)[0];
    }

    Array.prototype.random  =  Array.prototype.random  ||  function (Mode, Pop) {
        if (! Mode)  return Probability_Random(this);

        if (! Pop) {
            if (! (this.Random_Queue && this.Random_Queue.length))
                this.Random_Queue = [ ].concat(this);
            return Sequence_Random(this.Random_Queue);
        }
        return Sequence_Random(this);
    };

/* ---------- String Trim ---------- */

    String.prototype.trim  =  String.prototype.trim  ||  function () {
        return  this.replace(/(^\s*)|(\s*$)/g, '');
    };

/* ---------- Window Frame Traversing ---------- */

    BOM.getAllFrames  =  BOM.getAllFrames  ||  function () {
        var iSet = [ ],  iFrame = this.frames;

        for (var i = 0, j = 0;  i < iFrame.length;  i++) {
            iSet[j++] = iFrame[i];

            if (iFrame[i].frames.length)
                iSet = iSet.concat( arguments.callee.call( iFrame[i] ) );
        }

        return iSet;
    };

    BOM.eachFrame  =  BOM.eachFrame  ||  function () {
        var iFrame = [this].concat( BOM.getAllFrames.call(this) );

        for (var i = 0, _Return_; i < iFrame.length; i++)  try {

            _Return_ = arguments[0].call(iFrame[i], iFrame[i].document);

            if (_Return_ === false)  break;

            if (_Return_ !== undefined)  return _Return_;

        } catch (iError) { }
    };

/* ---------- Selected Text in Hole Page ---------- */

    BOM.getSelectedText  =  BOM.getSelectedText  ||  function () {
        return  BOM.eachFrame.call(this,  function (_DOM_) {
            var This_Tag = _DOM_.activeElement;

            if (_DOM_.selection)
                return _DOM_.selection.createRange().text;

            switch ( This_Tag.tagName.toLowerCase() ) {
                case 'input':       ;
                case 'textarea':    with (This_Tag)
                    return  value.slice(selectionStart, selectionEnd);
                case 'body':        ;
                default:            return _DOM_.getSelection().toString();
            }
        });
    };

})(top);



(function (BOM, iLanguage, iProxy) {

    var iKeyWord = BOM.getSelectedText().trim();

    if (iKeyWord)
        return BOM.open([
            'http://',  iProxy.random(true),
            '/search?newwindow=1&lr=lang_',  iLanguage,
            '&q=',  BOM.encodeURIComponent(iKeyWord)
        ].join(''), '_blank');

    if (BOM.confirm([
        "您未选中任何网页中的文字……",
        "『确定』进入问题反馈；『取消』即退出本工具。"
    ].join("\n\n\n"))) {
        BOM.prompt(
            "输入框中的是『运行环境』信息，请直接复制它们，按『确认』即可访问 原作者主页～",
            navigator.userAgent
        );
        BOM.open('http://www.fyscu.com/', '_blank');
    } else
        BOM.alert(
            "【Google 中文划词搜索 v1.3】\n\n(C)2013-2016  四川大学·飞扬俱乐部·研发部"
        );

})(top, 'zh-CN', [
    'sssis.com',
    'www.90r.org',
    'booo.so',
    'g.ttlsa.com',
    'lamjoeone.info',
    'g.net.co',
    'iguge.tk',
    'jiong.lu',
    'xie.lu',
    'ggncr.com',
    'everthis.com',
    'ppx.pw',
    'gugesou.com',
    'soso.red',
    '521.pt',
    'google.sqzr.cc',
    'yue.pao.im',
    'da.pao.im',
    'g.xjliao.me',
    'gg.eeload.com',
    'repigu.com',
    'jinke.la',
    'glgoo.com',
    'googleforchina.com',
    '74.125.12.198',
    '209.116.186.219',
    '173.194.121.28',
    'g.hi18.cn'
]);