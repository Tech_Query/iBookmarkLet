(function (BOM) {

    var newURL = BOM.location.href.split('/');

    if (! newURL[0].match(/^(http|https|file|ftp):$/))
        return alert("当前网址所指向的不是有效的网络资源，无法回溯……") || false;

    var iLen = newURL.length;
    if (newURL[iLen-1] === '') {
        newURL.pop();
        iLen--;
    }
    if (iLen > 3) newURL.splice(-1, 1);
    else {
        if (newURL[2].slice(0, 4) === 'www.')
            return alert("当前网址已是该网站的顶端，不能再回溯了~");
        var new_URL = newURL[2].split('.');
        new_URL.splice(0, 1);
        if (new_URL.length < 3) new_URL.splice(0, 0, 'www');
        newURL[2] = new_URL.join('.');
    }
    BOM.location.href = newURL.join('/');

})(top);