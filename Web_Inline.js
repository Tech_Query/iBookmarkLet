//
//                >>>  Web Inliner  <<<
//
//
//      [Version]    v0.2  (2016-03-30)  Beta
//
//      [Require]    JavaScript v1.8+  (ECMAScript 5)
//
//      [Usage]      Make CSS, Image etc inline to
//
//                   a single HTML file.
//
//
//            (C)2016    shiy2008@gmail.com
//


javascript:  (function (BOM, DOM) {

    /* ---------- 生效样式 ---------- */

    function Tag_Computed_CSS(iDOM) {
        return JSON.parse(JSON.stringify(
            iDOM.ownerDocument.defaultView.getComputedStyle(iDOM)
        ));
    }

    /* ---------- 原始样式 ---------- */

    var Tag_Style = { },  iSandBox = DOM.createElement('iframe');

    iSandBox.style.display = 'none';
    DOM.body.appendChild( iSandBox );

    function Tag_Default_CSS(iTagName) {
        var _DOM_ = iSandBox.contentWindow.document;

        if (! Tag_Style[iTagName]) {
            var iDefault = _DOM_.createElement( iTagName );
            _DOM_.body.appendChild( iDefault );
            Tag_Style[iTagName] = Tag_Computed_CSS(iDefault);
            _DOM_.body.removeChild( iDefault );
        }
        return Tag_Style[iTagName];
    }

    /* ---------- 变更样式 ---------- */

    function Tag_Customed_CSS() {
        var iCustomed = { },
            iComputed = Tag_Computed_CSS( arguments[0] ),
            iDefault = Tag_Default_CSS( arguments[0].tagName.toLowerCase() );

        for (var iAttr in iComputed)
            if (
                isNaN(Number( iAttr ))  &&
                (! iAttr.match(/^(moz|webkit|ms)/))  &&
                (iComputed[iAttr] != iDefault[iAttr])  &&
                (! iAttr.match(/width|height/i))
            )
                iCustomed[iAttr] = iComputed[iAttr];

        return iCustomed;
    }

    /* ---------- 内联化核心 ---------- */

    BOM.CSS_Inline = function () {
        var iStyle = Tag_Customed_CSS( arguments[0] );

        for (var iAttr in iStyle)
            arguments[0].style[iAttr] = iStyle[iAttr];
    };

    BOM.Image_Inline = function (iDOM, iWaterMark) {
        var _Image_ = new Image(),  iCanvas = DOM.createElement('canvas');

        _Image_.crossOrigin = '';
        var iContext = iCanvas.getContext('2d');

        _Image_.onload = function () {
            iCanvas.width = _Image_.width;
            iCanvas.height = _Image_.height;
            iContext.drawImage(_Image_, 0, 0);

            if (iWaterMark) {
                iContext.font = '20px sans-serif';
                iContext.fillStyle = 'white';
                iContext.fillText(iWaterMark,  10,  _Image_.height - 15);
            }
            iDOM.src = iCanvas.toDataURL('image/png');
            _Image_ = null;
        };
        _Image_.src = iDOM.src;
    };

    BOM.Web_Inline = function () {
        var iTag = arguments[0].querySelectorAll('*');

        for (var i = 0;  i < iTag.length;  i++)
            switch ( iTag[i].tagName.toLowerCase() ) {
                case 'meta':      ;
                case 'style':     ;
                case 'script':    ;
                case 'iframe':
                    iTag[i].parentNode.removeChild( iTag[i] );    break;
                case 'img':
                    this.Image_Inline(iTag[i], arguments[1]);     break;
                default:
                    this.CSS_Inline( iTag[i] );
            }
        return arguments[0].innerHTML.trim();
    };

    /* ---------- 使用流程 ---------- */

    BOM.Web_Inline(DOM.body, BOM.prompt("图片水印文字："));

    BOM.setTimeout(function () {
        DOM.body.textContent = DOM.body.innerHTML;
        BOM.alert("请全选、复制当前显示的所以代码~");
    }, 1000);

})(self, self.document);