(function (BOM, DOM, $) {

    var CSS_File = DOM.styleSheets,
        CopyRight = '(C)2014-2015  test_32@fyscu.com';

    if (! $) {
        BOM.alert(
            'Please run This Tool in IE 8+ (Standard Mode) or a Modern Web Browser.' + "\n\n" +
            CopyRight
        );
        return false;
    } else if (! console) {
        BOM.alert(
            'Please run This Tool with JavaScript Console opened.' + "\n\n" +
            CopyRight
        );
        return false;
    }

    function URL_Path(URL) {
        if ( URL.match(/^\w+:\/\//) ) {
            URL = URL.split('/').slice(3);
            URL.unshift('.');
            URL = URL.join('/');
        }
        return URL;
    }

    function SelectorCheck(Selector) {
        Selector = Selector.split(',');
        var _NU_ = [ ];

        for (var k = 0;  k < Selector.length;  k++)
            if (! $( Selector[k].trim().split(':')[0] ).length)
                _NU_.push(Selector[k]);

        return _NU_;
    }

    function CSS_Rule_Each(CSS_Rule_Group) {
        try {
           var  CSS_Rule = CSS_Rule_Group.cssRules || CSS_Rule_Group.rules;
        } catch (Err) { }

        var Selector = '',
            NoUsage = [ ],
            Test_Result = {
                media:         CSS_Rule_Group.media.mediaText,
                mediaRules:    [ ],
                fontsRules:    [ ]
            };
        if (CSS_Rule) {
            for (var j = 0, _Rule_;  j < CSS_Rule.length;  j++) {
                _Rule_ = CSS_Rule[j];
                switch (_Rule_.type) {
                    case 1:    {    //  CSSStyleRule
                        _Rule_ = SelectorCheck(_Rule_.selectorText);
                        if (_Rule_.length)  NoUsage.push(_Rule_);
                    }  break;
                    case 4:        //  CSSMediaRule
                        Test_Result.mediaRules.push( arguments.callee(_Rule_)[0] );  break;
                    case 5:        //  CSSFontFaceRule
                        Test_Result.fontsRules.push({
                            fontFamily:    _Rule_.style.fontFamily,
                            src:           _Rule_.style.src
                        });
                }
            }
            Test_Result.WasteRate = ((NoUsage.length / j) * 100).toFixed(2) + '%';
        }
        return [Test_Result, NoUsage];
    }

    for (var i = 0, _File_, _Result_ = [ ];  i < CSS_File.length;  i++) {
        _Result_[i] = CSS_Rule_Each(CSS_File[i]);
        var _File_ = _Result_[i].shift();

        _File_.href = CSS_File[i].href;
        if (_File_.WasteRate && _File_.href)
            _File_.href = URL_Path(_File_.href);
        _File_.element = CSS_File[i].ownerNode;
        console.log(_File_);
    }
    console.log(_Result_);

})(self,  self.document,  self.document.querySelectorAll);